import "core-js";

import {describe, it} from "mocha";
import {createTypesFor} from "../../src";
import {expect} from "chai";

class ExampleTypes
{
    public static Unknown = 0;
    public static One = 1;
    public static Ten = 10;
    public static Hundred = 100;
}

class ExampleModel
{ 
    public typeValue = 0;
    public typeName = "";
}

describe('Create Types Tests', function() {
    it('should create object with default values for each type', function(done) {
        
        const createdTypes = createTypesFor(ExampleTypes, 0, ExampleModel, (model, type, key) => {
            model.typeValue = type;
            model.typeName = key;
            console.log("type", type, key);
        });

        expect(createdTypes).to.be.an('array');
        expect(createdTypes.length).to.equal(4);

        expect(createdTypes).to.deep.include({ typeValue: 0, typeName: "Unknown" });
        expect(createdTypes).to.deep.include({ typeValue: 1, typeName: "One" });
        expect(createdTypes).to.deep.include({ typeValue: 10, typeName: "Ten" });
        expect(createdTypes).to.deep.include({ typeValue: 100, typeName: "Hundred" });
        
        done();
    });

    it('should create object with default values for each type when skipping', function(done) {
        
        const createdTypes = createTypesFor(ExampleTypes, 10, ExampleModel, (model, type, key) => {
            model.typeValue = type;
            model.typeName = key;
            console.log("type", type, key);
        });

        expect(createdTypes).to.be.an('array');
        expect(createdTypes.length).to.equal(2);

        expect(createdTypes).to.deep.include({ typeValue: 10, typeName: "Ten" });
        expect(createdTypes).to.deep.include({ typeValue: 100, typeName: "Hundred" });
        
        done();
    });
});