import {ICharacter} from "../models/character/icharacter";

export interface ICharacterFactory
{
    CurrentVersion: string;
    create(): ICharacter;
}
