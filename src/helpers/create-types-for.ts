export function createTypesFor<T>(typeClass: any, startAt: number = 0, creator: new() => T, populator: (model: T, type: number, key?: string) => void): Array<T>
{
    var createdTypes: Array<T> = [];
    var types = Object.keys(typeClass);
    types.forEach((typeKey: any) => {
        const typeValue = typeClass[typeKey];
        if(typeValue < startAt){ return; }

        const createdType = new creator();
        populator(createdType, typeValue, typeKey);
        createdTypes.push(createdType);
    });
    return createdTypes;
}