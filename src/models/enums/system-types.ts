
/**
 * @object SystemTypes
 * An enum-like object for encapsulating game system types
 *
 * @prop [Number] Unknown An unknown game system
 * @prop [Number] Dnd4ed The dungeons & dragons 4th edition game system
 * @prop [Number] Saga The starwars saga edition game system
 * @prop [Number] Wfrp The warhammer fantasy roleplaying game system
 * @prop [Number] Pathfinder The pathfinder game system
 * @prop [Number] Asoiaf A Song of Ice and Fire game system
 * @prop [Number] Dnd35ed The dungeons & dragons 3.5 edition game system
 * @prop [Number] Dnd5ed The dungeons & dragons 5th edition (NEXT) game system
 * @prop [Number] Dungeonworld The dungeon world game system
 */
export class SystemTypes
{
    public static Unknown = 0;
    public static Dnd4ed = 1;
    public static Saga = 2;
    public static Wfrp = 3;
    public static Pathfinder = 4;
    public static Asoiaf = 5;
    public static Dnd35ed = 6;
    public static Dnd5ed = 7;
    public static Dungeonworld = 11;
}
