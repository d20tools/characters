/**
 * @constructor Item
 * Contains all data that makes up an item
 */
import {maxLength, number, required} from "@treacherous/decorators";

export class Item
{
    /**
     * @object Name
     * The name of the item
     */
    @required()
    @maxLength(50)
    public Name = "";

    /**
     * @object Weight
     * The weight of the item
     */
    @number()
    public Weight = "";

    /**
     * @object Value
     * The value of the item
     */
    @maxLength(30)
    public Value = "";

    /**
     * @object Amount
     * The amount of the item held
     */
    @number()
    public Amount = "";
}