import {required, minLength, maxLength, withRuleset, withRulesetForEach} from '@treacherous/decorators';
import {ICharacter} from "./icharacter";
import {Characteristics} from './characteristics';
import {Personality} from "./personality";
import {Notes} from "./notes";
import {Inventory} from './inventory';
import {Equipment} from "./equipment";
import {Language} from "./language";

/**
 * @constructor Character
 * Represents a Basic character template, this can be inherited from or ignored and manually implemented
 */
export abstract class DefaultCharacter implements ICharacter
{
    /**
     * @object Version
     * The current version of this model
     */
    public abstract Version: string;

    /**
     * @object SystemType
     * The system type of the character
     */
    public abstract SystemType: number;

    public IsPrivate = false;

    /**
     * @object AccountId
     * The account id this character belongs to
     */
    public AccountId = "";

    /**
     * @object CharacterId
     * The id of this character
     */
    public LocalId = "";

    /**
     * @object PersistedId
     * The persisted id of this character
     */
    public PersistedId = "";

    /**
     * @object LastUpdated
     * The datetime of the last online update of this character
     */
    public LastUpdated = new Date();

    /**
     * @object Avatar
     * The avatar data for this character, the base 64 string
     */
    public Avatar = "";

    /**
     * @object Name
     * The name for this character
     */
    @required()
    @minLength(2)
    @maxLength(50)
    public Name = "";

    /**
     * @object Gender
     * The gender data of this character
     */
    @required()
    @maxLength(30)
    public Gender = "";

    /**
     * @object MetaData
     * The meta data stored with this character
     *
     * @note MetaData is a JSON object, so should be added to as so
     */
    public MetaData = {};

    /**
     * @object Characteristics
     * The characteristics model
     */
    @withRuleset(Characteristics)
    public Characteristics = new Characteristics();

    /**
     * @object Personality
     * The personality model
     */
    @withRuleset(Personality)
    public Personality = new Personality();
    /**
     * @object Notes
     * The notes model
     */
    @withRuleset(Notes)
    public Notes = new Notes();

    /**
     * @object Inventory
     * The inventory model
     */
    @withRuleset(Inventory)
    public Inventory = new Inventory();

    /**
     * @object Equipment
     * The equipment model
     */
    @withRuleset(Equipment)
    public Equipment = new Equipment();

    @withRulesetForEach(Language)
    public Languages: Language[] = [];
}