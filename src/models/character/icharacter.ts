import {Personality} from "./personality";
import {Characteristics} from "./characteristics";
import {Notes} from "./notes";

export interface ICharacter
{
    Version: string;
    SystemType: number;
    MetaData: any;

    IsPrivate: boolean;
    Name: string;
    Gender: string;
    Avatar: string;
    AccountId: string;
    LocalId: string;
    PersistedId: string;
    LastUpdated: any;

    Characteristics: Characteristics;
    Personality: Personality;
    Notes: Notes;
}
