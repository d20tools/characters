/**
 * @constructor Notes
 * Contains all the character notes entered by the player
 */
import {maxLength} from "@treacherous/decorators";

export class Notes
{
    /**
     * @object General
     * This is used to store general notes
     */
    @maxLength(5000)
    public General = "";

    /**
     * @object Quests
     * This is used to store quest related notes
     */
    @maxLength(5000)
    public Quests = "";

    /**
     * @object Personal
     * This is used to store personal character based information
     */
    @maxLength(5000)
    public Personal = "";
}