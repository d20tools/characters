/**
 * @constructor Characteristics
 * Contains characteristic based information for a character
 */
import {maxLength} from "@treacherous/decorators";

export class Characteristics
{
    /**
     * @object Age
     * The age of the character in years
     */
    @maxLength(30)
    public Age = "";

    /**
     * @object Weight
     * The weight of the character
     */
    @maxLength(30)
    public Weight = "";

    /**
     * @object Height
     * The height of the character
     */
    @maxLength(30)
    public Height = "";

    /**
     * @object HairColour
     * The hair colour of the character
     */
    @maxLength(30)
    public HairColour = "";

    /**
     * @object EyeColour
     * The eye colour of the character
     */
    @maxLength(30)
    public EyeColour = "";

    /**
     * @object PhysicalDescription
     * The physical description of the character as another character would see him
     */
    @maxLength(1000)
    public PhysicalDescription = "";

}
