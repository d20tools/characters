import {Item} from "./item";
import {number, withRulesetForEach} from "@treacherous/decorators";

/**
 * @constructor Inventory
 * Contains items and inventory related details
 */
export class Inventory
{
    /**
     * @object MaxWeight
     * The maximum carrying weight for items in the inventory
     */
    @number()
    public MaxWeight = "";

    /**
     * @object TotalWeight
     * The current total weight of items in the inventory
     */
    @number()
    public TotalWeight = "";

    /**
     * @object Items
     * The array of items held within the players inventory
     */
    @withRulesetForEach(Item)
    public Items: Array<Item> = [];
}