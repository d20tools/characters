/**
 * @constructor Personality
 * Contains all the character's personality based information
 */
import {maxLength} from "@treacherous/decorators";

export class Personality
{
    /**
     * @object Background
     * The character's background information
     */
    @maxLength(5000)
    public Background = "";

    /**
     * @object Traits
     * The character's traits
     */
    @maxLength(2000)
    public Traits = "";

    /**
     * @object Goals
     * The character's goals
     */
    @maxLength(2000)
    public Goals = "";

    /**
     * @object Achievements
     * The character's achievements
     */
    @maxLength(2000)
    public Achievements = "";

    /**
     * @object Motivations
     * The character's motivations
     */
    @maxLength(2000)
    public Motivations = "";

    /**
     * @object Likes
     * The character's likes
     */
    @maxLength(2000)
    public Likes = "";

    /**
     * @object Dislikes
     * The character's dislikes
     */
    @maxLength(2000)
    public Dislikes = "";
}