/**
 * @constructor Equipment
 * Contains characters equipped items
 *
 * @note To keep things simple equipment is stored as text not item objects
 */
import {maxLength} from "@treacherous/decorators";

export class Equipment
{
    /**
     * @object Head
     * The equipped item in the Head slot
     */
    @maxLength(50)
    public Head = "";

    /**
     * @object Shoulders
     * The equipped item in the Shoulders slot
     */
    @maxLength(50)
    public Shoulders = "";

    /**
     * @object Back
     * The equipped item in the Back slot
     */
    @maxLength(50)
    public Back = "";

    /**
     * @object Chest
     * The equipped item in the Chest slot
     */
    @maxLength(50)
    public Chest = "";

    /**
     * @object Waist
     * The equipped item in the Waist slot
     */
    @maxLength(50)
    public Waist = "";

    /**
     * @object Legs
     * The equipped item in the Legs slot
     */
    @maxLength(50)
    public Legs = "";

    /**
     * @object Feet
     * The equipped item in the Feet slot
     */
    public Feet = "";

    /**
     * @object Hands
     * The equipped item in the Hands slot
     */
    @maxLength(50)
    public Hands = "";

    /**
     * @object LeftHand
     * The equipped item in the Left Hand slot
     */
    @maxLength(50)
    public LeftHand = "";

    /**
     * @object RightHand
     * The equipped item in the Right Hand slot
     */
    @maxLength(50)
    public RightHand = "";

    /**
     * @object Other1
     * The equipped item in the Other1 slot
     */
    @maxLength(50)
    public Other1 = "";

    /**
     * @object Other2
     * The equipped item in the Other2 slot
     */
    @maxLength(50)
    public Other2 = "";

    /**
     * @object Other3
     * The equipped item in the Other3 slot
     */
    @maxLength(50)
    public Other3 = "";

    /**
     * @object Other4
     * The equipped item in the Other4 slot
     */
    @maxLength(50)
    public Other4 = "";
}