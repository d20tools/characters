/**
 * @constructor Language
 * Contains language based information for a character
 */
import {maxLength, required} from "@treacherous/decorators";

export class Language
{
    /**
     * @object Language
     * The language the character knows
     */
    @required()
    @maxLength(50)
    public Language = "";
}
